#!/bin/bash

if [[ "$CI_JOB_ID" == "" ]]; then
  echo "Not running in GitLab CI" 1>&2
  exit
fi

#if [[ "$CI_COMMIT_REF_NAME" == "master" ]]; then
#  echo "Building master branch, skipping"
#  exit 0
#fi

ver="$CI_COMMIT_SHORT_SHA"
ci_url="https://$CI_PROJECT_TITLE"
ci_baseurl=""

echo "url: \"$ci_url\""
echo "baseurl: \"$ci_baseurl\""
echo "v: \"$ver\""

sed -i 's|^baseurl:.*$|baseurl: "'"$ci_baseurl"'"|' _config.yml
sed -i 's|^url:.*$|url: "'"$ci_url"'"|' _config.yml
sed -i "s|STYLESHEETVER|$ver|g" _config.yml

if [[ "$CI_PROJECT_TITLE" == "depau.gay" ]]; then
  sed -i 's|dotgay:.*|dotgay: true|g' _config.yml
else
  sed -i 's|dotgay:.*|dotgay: false|g' _config.yml
fi
